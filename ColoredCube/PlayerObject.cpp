
#include "PlayerObject.h"

PlayerObject::PlayerObject()
{
	collision = false;
	hasKey = false;
}

void PlayerObject::init(TexBox *b, float r, Vector3 pos, Vector3 vel, Vector3 lookAt, float sp, float s)
{
	GameObject::init(b,r,pos,vel,sp,s);

	//not sure if velocity is what should be initialized for camera direction.
	camera.init(pos, vel, lookAt);
	camera.setSpeed(sp);
	originalPos = pos;
}

void PlayerObject::update(float dt)
{
	camera.update(dt);
	GameObject::setPosition(camera.getPosition());
	GameObject::update(dt);
}

//sets the collision vector to be of the appropraite value
void PlayerObject::bounce(GameObject *go) {
	Vector3 collisionVector = go->getPosition() - getPosition();

	//normalize the collision vec and make it super small so that it's not shaky
	collisionVector = *D3DXVec3Normalize(&collisionVector, &collisionVector)/200;

	int count = 30;
	do {
		setPosition(getPosition() - collisionVector);
		count--;
	} while (GameObject::collided(go) && count);
}