#include "mapMaker.h"


mapMaker::mapMaker(void)
{
	width = 0;
	height = 0;
	filename = "";
	map = "";
}

mapMaker::mapMaker(string fn) {
	filename = fn;

	fin.open(filename);
	if (!fin.good())
		throw(GameError(gameErrorNS::FATAL_ERROR, "File Name Error"));

	fin >> width;
	fin >> height;
	fin.get();
	fin.get();

	readInMap();
	fin.close();
}

mapMaker::~mapMaker(void)
{
}

void mapMaker::setFilename(string fn) {
	filename = fn;
	readInMap();
}

void mapMaker::readInMap() {
	char c;
	while (!fin.eof()) {
		c = fin.get();
		if (c != '\n')
			map += c;
	}
}
