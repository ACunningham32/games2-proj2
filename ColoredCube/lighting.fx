//=============================================================================
// lighting.fx by Frank Luna (C) 2008 All Rights Reserved.
//
// Transforms and lights geometry.
//=============================================================================

#include "lighthelper.fx"
 
cbuffer cbPerFrame
{
	Light gLight1;
	int gLightType1; 
	float3 gEyePosW;
};

int flip;

cbuffer cbPerObject
{
	float4x4 gWorld;
	float4x4 gWVP;
	float4x4 gTexMtx;
};

Texture2D gDiffuseMap;
Texture2D gSpecMap;

SamplerState gTriLinearSam
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
};

struct VS_IN
{
	float3 posL    : POSITION;
	float3 normalL : NORMAL;
	float4 diffuse : DIFFUSE;
	float4 spec    : SPECULAR;
	float2 texC    : TEXCOORD;
};

struct VS_OUT
{
	float4 posH    : SV_POSITION;
    float3 posW    : POSITION;
    float3 normalW : NORMAL;
    float4 diffuse : DIFFUSE;
    float4 spec    : SPECULAR;
	float2 texC    : TEXCOORD;
	// add for ambient
};



VS_OUT VS(VS_IN vIn)
{
	VS_OUT vOut;
	
	// Transform to world space space.
	vOut.posW    = mul(float4(vIn.posL, 1.0f), gWorld);
	vOut.normalW = mul(float4(vIn.normalL, 0.0f), gWorld);
		
	// Transform to homogeneous clip space.
	vOut.posH = mul(float4(vIn.posL, 1.0f), gWVP);
	
	// Output vertex attributes for interpolation across triangle.
	vOut.diffuse = vIn.diffuse;
	vOut.spec    = vIn.spec;

	vOut.texC  = mul(float4(vIn.texC, 0.0f, 1.0f), gTexMtx);
	
	return vOut;
}
 
float4 PS(VS_OUT pIn) : SV_Target
{
	// Interpolating normal can make it not be of unit length so normalize it.
    
	//float3 normalW = normalize(pIn.normalW);
   
   	float4 diffuse = gDiffuseMap.Sample( gTriLinearSam, pIn.texC );
	float4 spec    = gSpecMap.Sample( gTriLinearSam, pIn.texC );

	SurfaceInfo v = {pIn.posW, pIn.normalW, diffuse, spec};

    
    float3 litColor = float3(0.0f, 0.0f, 0.0f);
    if ( gLightType1 == 0 ) // Parallel
    {
		litColor = ParallelLight(v, gLight1, gEyePosW);
		//litColor = dot(litColor, float3(0.3, 0.59, 0.11));
		litColor = dot(litColor, float3(1.0, 1.0, 1.0));
    }
    else if ( gLightType1 == 1 ) // Point
    {
		litColor = PointLight(v, gLight1, gEyePosW);
	}
	else if ( gLightType1 == 2 ) // Spot
	{
		litColor = Spotlight(v, gLight1, gEyePosW);
		//litColor = dot(litColor, float3(0.0, 0.59, 0.41));
	}
	else 
	{
		litColor = ParallelLight(v, gLight1, gEyePosW);
		litColor = dot(litColor, float3(0.3, 0.59, 0.11));
	}

	if (flip == 1) {
		return float4(1 - diffuse.r, 1 - diffuse.g, 1 - diffuse.b, diffuse.a);
	}
	
    return float4(litColor, diffuse.a);
}

technique10 LightTech
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}