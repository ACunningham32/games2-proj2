#pragma once
// Andrew Cunningham
//
// This saves the whole map file as a string.  This way we can change the
// size of the map without worrying about 2D dynamic arrays.  We just
// need to save the width and height the file gives us in the main game
// file to properly make the maze.

#include "constants.h"
#include "gameError.h"

#include <string>
#include <fstream>
using std::string;
using std::ifstream;

class mapMaker
{
private:
	int width;
	int height;

	string map;
	string filename;
	ifstream fin;

	void readInMap(); // cannot be called without intializing file name

public:
	mapMaker(void);
	mapMaker(string fn);
	~mapMaker(void);

	int getWidth()  { return width; }
	int getHeight() { return height;}

	void setFilename(string fn);
	string getFilename() {return filename;}

	string getMap() {return map;}
};

