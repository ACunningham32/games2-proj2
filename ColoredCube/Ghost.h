#pragma once

#include "d3dUtil.h"
#include "GameObject.h"
#include "Box.h"
#include "Camera.h"
#include "constants.h"
#include "point.h";
#include "PlayerObject.h"
#include "audio.h"

#define maxPatternSteps 5

struct PatternStepInfo {
	PatternStepInfo(): action(NONE), timeForStep(0) { }
	PatternStepInfo(PATTERN_STEP_ACTION a, float t): action(a), timeForStep(t) { }
	PATTERN_STEP_ACTION action;
	float timeForStep;
};


class Ghost : public GameObject
{
public:
	Ghost();
	void update(float dt);
	void init(TexBox *b, float r, Vector3 pos, Vector3 vel, float sp, float s, PlayerObject* player, float trackRadius, PatternStepInfo p[]);
	void setDirection(Vector3 v) { direction = v; }
	void setAngle(float a) { angle = a; }
	void setPatterns(PatternStepInfo patterns[]) {
		for (int i = 0; i < maxPatternSteps; i++)
			patternSteps[i] = patterns[i];
	}

	void setAudio(Audio* a) {audio = a;}
	void bounce(GameObject *go);
	bool getShouldTrack() {return shouldTrack();}

private:
	void vectorTrack();
	void returnToOrigin();
	void updatePatterns(float dt);
	bool shouldTrack() { 
		if (getSpeed() != GHOST_LIGHT_SPEED)
			setSpeed(GHOST_CHASE_SPEED);
		return D3DXVec3Length(&(player->getPosition() - origin )) <= trackRadius;
	}
	PatternStepInfo patternSteps[maxPatternSteps];
	PATTERN_STEP_ACTION currentAction;
	bool shouldReturn;
	float timeInStep;
	int patternIndex;
	float trackRadius;
	PlayerObject* player;
	float angle;
	Vector3 direction;
	Vector3 origin;
	Audio* audio;
};