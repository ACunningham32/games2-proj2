
#pragma once


#include "d3dUtil.h"
#include "GameObject.h"
#include "Box.h"
#include "Camera.h"
#include "constants.h"
#include "point.h";

class PlayerObject : public GameObject
{
public:
	PlayerObject();
	void init(TexBox *b, float r, Vector3 pos, Vector3 vel,Vector3 lookAt, float sp, float s);
	void update(float dt);
	void setSpeed(float sp) { camera.setSpeed(sp); GameObject::setSpeed(sp); }
	void setPosition (Vector3 pos) { 
		GameObject::setPosition(pos);
		camera.setLookAt(camera.getLookAt() + pos - camera.getPosition());
		camera.setPosition(pos);			
	}
	Camera camera;
	void bounce(GameObject* go);
	void setCollision(bool b) { collision = b; }

	bool getKey(){return hasKey;}
	void setKey(bool k){hasKey = k;}

		Vector3 originalPos;

private:
	bool collision;
	bool hasKey;
};