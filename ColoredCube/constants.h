// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 constants.h v1.1

#pragma once

#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//=============================================================================
// Function templates for safely dealing with pointer referenced items.
// The functions defined by these templates may be called using a normal
// function call syntax. The compiler will create a function that replaces T
// with the type of the calling parameter.
//=============================================================================
// Safely release pointer referenced item
template <typename T>
inline void safeRelease(T& ptr)
{
	if (ptr)
	{ 
		ptr->Release(); 
		ptr = NULL;
	}
}
#define SAFE_RELEASE safeRelease            // for backward compatiblility

// Safely delete pointer referenced item
template <typename T>
inline void safeDelete(T& ptr)
{
	if (ptr)
	{ 
		delete ptr; 
		ptr = NULL;
	}
}
#define SAFE_DELETE safeDelete              // for backward compatiblility

// Safely delete pointer referenced array
template <typename T>
inline void safeDeleteArray(T& ptr)
{
	if (ptr)
	{ 
		delete[] ptr; 
		ptr = NULL;
	}
}
#define SAFE_DELETE_ARRAY safeDeleteArray   // for backward compatiblility

// Safely call onLostDevice
template <typename T>
inline void safeOnLostDevice(T& ptr)
{
	if (ptr)
		ptr->onLostDevice(); 
}
#define SAFE_ON_LOST_DEVICE safeOnLostDevice    // for backward compatiblility

// Safely  call onResetDevice
template <typename T>
inline void safeOnResetDevice(T& ptr)
{
	if (ptr)
		ptr->onResetDevice(); 
}
#define SAFE_ON_RESET_DEVICE safeOnResetDevice  // for backward compatiblility

//=============================================================================
//                  Constants
//=============================================================================


const unsigned int MAP_WIDTH = 20;
const unsigned int MAP_HEIGHT = 20;

const unsigned int NUM_GHOSTS = 2;
const float GHOST_RADIUS = 10.0f;

// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
//LPCWSTR WAVE_BANK[]  = "audio\\Win\\WavesExample1.xwb";
const char WAVE_BANK[]  = "audio\\Win\\WavesExample1.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\SoundsExample1.xsb";
//LPCWSTR  SOUND_BANK[] = "audio\\Win\\SoundsExample1.xsb";
// audio cues
const char BEEP1[] = "beep1";
const char BEEP2[] = "beep2";
const char BEEP3[] = "beep3";
const char BEEP4[] = "beep4";
const char HIT[]   = "hit";
const char LASER[]   = "laser";
const char KEYS[]   = "keys";
const char OPEN[]   = "door_open";
const char BOOM[]   = "boom";
const char FAIL[]   = "fail";
const char BKG[]   = "bkg";
const char SELECT[]   = "select";
const char FOOTSTEPS1[] = "footsteps1";
const char FOOTSTEPS2[] = "footsteps2";
const char FOOTSTEPS3[] = "footsteps3";
const char MENU_MUSIC[] = "sadPiano";
const char GAME_MUSIC[] = "scaryMusic";
const char GHOST_CHASE1[] = "ghostChase1";
const char GHOST_CHASE2[] = "ghostChase2";
const char GHOST_CHASE3[] = "ghostChase3";
const char PLAYER_DIE[] = "playerDie";

// States
enum GameStates {intro, gamePlay, end, readyToPlay};

#define Vector3 D3DXVECTOR3
#define Matrix D3DXMATRIX
#define Identity D3DXMatrixIdentity
#define Translate D3DXMatrixTranslation
#define RotateX D3DXMatrixRotationX
#define RotateY D3DXMatrixRotationY
#define RotateZ D3DXMatrixRotationZ
#define ToRadian D3DXToRadian
#define ToDegree D3DXToDegree
#define Transform D3DXVec3TransformCoord


enum PATTERN_STEP_ACTION {NONE, UP, DOWN, LEFT, RIGHT, TRACK, RETURN};

const float GHOST_LIGHT_SPEED  = 1.0f;
const float GHOST_NORMAL_SPEED = 3.0f;
const float GHOST_CHASE_SPEED  = 3.0f;
