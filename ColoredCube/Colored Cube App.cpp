//=============================================================================
// Color Cube App.cpp by Frank Luna (C) 2008 All Rights Reserved.
//
// Demonstrates coloring.
//
// Controls:
//		'A'/'D'/'W'/'S' - Rotate 
//
//=============================================================================

#include "d3dApp.h"
#include "Box.h"
#include "mapMaker.h"
#include "Line.h"
#include "Axes.h"
#include "Vertex.h"
#include "Triangle.h"
#include "Quad.h"
#include "Input.h"
#include "GameObject.h"
#include "PlayerObject.h"
#include "audio.h"
#include "winerror.h"
#include "TimeBuffer.h"
#include "Ghost.h"

#include "Camera.h"
#include "point.h"
#include "Light.h"

#include <sstream>

Vector3 PLAYER_START_LVL1 = Vector3(-1.5, 0, 2);
Vector3 INITIAL_LOOKAT    = Vector3(0,0,2);

class ColoredCubeApp : public D3DApp
{

public:
	ColoredCubeApp(HINSTANCE hInstance);
	~ColoredCubeApp();

	void initApp();
	void onResize();
	void updateScene(float dt);
	void drawScene();
	void restartGame();
	bool lightIsOnGhost(Ghost* ghost);
	void getMazeDimensions(mapMaker maze);
	void buildMaze(mapMaker maze);
	void goBackToStart();
private:

	void buildFX();
	void buildVertexLayouts();

	Input* input;
	Audio* audio;

	TexBox mPlayer;
	TexBox mBox;
	TexBox floorBox;
	TexBox ghostBox;
	TexBox keyBox;
	TexBox markerBox;

	//Quad mFloor;

	PlayerObject playerObject;
	GameObject floor;
	GameObject ceiling;
	GameObject key;
	GameObject key2;
	Ghost ghosts[NUM_GHOSTS];
	bool ghostChasing;

	//ghost pattern steps
	GameObject door;
	GameObject door2;

	GameObject teenMarker;
	GameObject childrensMarker;
	GameObject fictionMarker;
	GameObject nonfictionMarker;
	GameObject historyMarker;
	GameObject religionMarker;

	GameObject teenMarker2;
	GameObject childrensMarker2;
	GameObject fictionMarker2;
	GameObject nonfictionMarker2;
	GameObject historyMarker2;
	GameObject religionMarker2;

	GameObject splashScreen;

	GameObject* mazeBlocks;

	Light mLights[3];
	int mLightType; // 0 (parallel), 1 (point), 2 (spot)

	mapMaker maze1;

	ID3D10Effect* mFX;
	ID3D10EffectTechnique* mTech;
	ID3D10InputLayout* mVertexLayout;
	ID3D10EffectMatrixVariable* mfxWVPVar;

	// Texture stuff
	ID3D10ShaderResourceView* mStoneMapRV;

	// Lighting
	ID3D10EffectMatrixVariable* mfxWorldVar;
	ID3D10EffectVariable* mfxEyePosVar;
	ID3D10EffectVariable* mfxFlashLightVar;
	ID3D10EffectVariable* mfxSpotLightVar;
	ID3D10EffectVariable* mfxFlipGhostVar;
	ID3D10EffectScalarVariable* mfxLightType;

	ID3D10ShaderResourceView* mDiffuseMapRV;
	ID3D10ShaderResourceView* floorTex;
	ID3D10ShaderResourceView* ceilingTex;
	ID3D10ShaderResourceView* ghostTex;
	ID3D10ShaderResourceView* doorTex;
	ID3D10ShaderResourceView* keyTex;

	ID3D10ShaderResourceView* religionTex;
	ID3D10ShaderResourceView* nonfictionTex;
	ID3D10ShaderResourceView* fictionTex;
	ID3D10ShaderResourceView* teenTex;
	ID3D10ShaderResourceView* historyTex;
	ID3D10ShaderResourceView* childrensTex;

	ID3D10ShaderResourceView* splashTex;


	ID3D10ShaderResourceView* mSpecMapRV;

	ID3D10EffectShaderResourceVariable* mfxDiffuseMapVar;
	ID3D10EffectShaderResourceVariable* mfxSpecMapVar;
	ID3D10EffectMatrixVariable* mfxTexMtxVar;

	D3DXMATRIX mView;
	D3DXMATRIX mProj;
	D3DXMATRIX mWVP;

	// Camera stuff
	Vector3 cameraPos;
	Vector3 lookAt;

	float mTheta;
	float mPhi;

	int score;
	std::wstring finalScore;
	std::wstring timeString;

	int maze1Height;
	int maze1Width;
	int blockCounter;

	bool isOnMenu;
	bool didWin;

	// Timer
	float timer;

	int x;

};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif


	ColoredCubeApp theApp(hInstance);

	theApp.initApp();

	return theApp.run();
}

ColoredCubeApp::ColoredCubeApp(HINSTANCE hInstance) : D3DApp(hInstance), mFX(0), mTech(0), mVertexLayout(0),religionTex(0), nonfictionTex(0), fictionTex(0), teenTex(0), historyTex(0), childrensTex(0),
	mfxWVPVar(0), mSpecMapRV(0), mDiffuseMapRV(0), splashTex(0),keyTex(0), doorTex(0), ghostTex(0), mfxTexMtxVar(0), ceilingTex(0),floorTex(0), mfxDiffuseMapVar(0), mfxSpecMapVar(0), mTheta(0.0f), mPhi(PI*0.25f), mStoneMapRV(0)
{
	D3DXMatrixIdentity(&mView);
	D3DXMatrixIdentity(&mProj);
	D3DXMatrixIdentity(&mWVP); 

	input = new Input();
	audio = new Audio();
}

ColoredCubeApp::~ColoredCubeApp()
{
	if( md3dDevice )
		md3dDevice->ClearState();

	ReleaseCOM(mFX);
	ReleaseCOM(mVertexLayout);
	//safeDelete(input);
	ReleaseCOM(mStoneMapRV);
	ReleaseCOM(mDiffuseMapRV);
	ReleaseCOM(floorTex);
	ReleaseCOM(ceilingTex);
	ReleaseCOM(ghostTex);
	ReleaseCOM(doorTex);
	ReleaseCOM(keyTex);
	ReleaseCOM(mSpecMapRV);
	ReleaseCOM(childrensTex);
	ReleaseCOM(fictionTex);
	ReleaseCOM(nonfictionTex);
	ReleaseCOM(historyTex);
	ReleaseCOM(religionTex);
	ReleaseCOM(teenTex);
	ReleaseCOM(splashTex);
}

void ColoredCubeApp::initApp()
{

	D3DApp::initApp();

	isOnMenu = true;
	didWin = false;

	//for the player and fist-person camera --- shouldn't be drawn, just for collision detection
	mPlayer.init(md3dDevice, 1.0f);

	mBox.init(md3dDevice, 1.0f);
	ghostBox.init(md3dDevice, 0.35f);
	keyBox.init(md3dDevice, 0.1f);
	markerBox.init(md3dDevice, 0.3f);


	playerObject.init(&mPlayer, 1.0, PLAYER_START_LVL1, Vector3(0,0,0),INITIAL_LOOKAT, 5.0f, 1.0f);
	playerObject.camera.setPerspective();

	splashScreen.init(&mBox, 1.0, Vector3(4,0,2), Vector3(0,0,0), 5.0f, 1.0f);
	splashScreen.setActive();

	

	key.init(&keyBox, 1.0, Vector3(0,0,0), Vector3(0,0,0), 0.0f, 1.0f);
	key2.init(&keyBox, 1.0, Vector3(0,0,0), Vector3(0,0,0), 0.0f, 1.0f);


	buildFX();
	buildVertexLayouts();

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"bookcase.dds", 0, 0, &mDiffuseMapRV, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"floor.dds", 0, 0, &floorTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"ceiling.dds", 0, 0, &ceilingTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"ghost.dds", 0, 0, &ghostTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"door.dds", 0, 0, &doorTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"gold.dds", 0, 0, &keyTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"fiction.dds", 0, 0, &fictionTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"nonfiction.dds", 0, 0, &nonfictionTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"childrens.dds", 0, 0, &childrensTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"history.dds", 0, 0, &historyTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"religion.dds", 0, 0, &religionTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"teen.dds", 0, 0, &teenTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"StacksSplashScreen.dds", 0, 0, &splashTex, 0 ));



#pragma region lightInitialization
	mLightType = 2;
	// Parallel light.
	mLights[0].dir      = D3DXVECTOR3(0.57735f, -0.57735f, 0.57735f);
	mLights[0].ambient  = D3DXCOLOR(0.19f, 0.11f, 0.7f, 1.0f);
	mLights[0].diffuse  = D3DXCOLOR(0.3f, 0.59f, 0.11f, 1.0f);
	mLights[0].specular = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	// Pointlight--position is changed every frame to animate.
	mLights[1].ambient  = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f);
	mLights[1].diffuse  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[1].specular = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[1].att.x    = 0.0f;
	mLights[1].att.y    = 0.1f;
	mLights[1].att.z    = 0.0f;
	mLights[1].range    = 50.0f;

	// Spotlight--position and direction changed every frame to animate.
	//mLights[2].ambient  = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f);
	//mLights[2].diffuse  = D3DXCOLOR(0.1f, 0.6f, 0.3f, 1.0f);
	//mLights[2].specular = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[2].ambient  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[2].diffuse  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[2].specular = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	mLights[2].att.x    = 1.0f;
	mLights[2].att.y    = 0.0f;
	mLights[2].att.z    = 0.0f;
	mLights[2].spotPow  = 24.0f;
	mLights[2].range    = 10000.0f;
#pragma endregion

	audio = new Audio();
	if (*WAVE_BANK != '\0' && *SOUND_BANK != '\0')  // if sound files defined
	{
		if (!audio->initialize()) {

		}
		/*if( FAILED( hr = audio->initialize() ) )
		{
		if( hr == HRESULT_FROM_WIN32( ERROR_FILE_NOT_FOUND ) )
		throw(GameError(gameErrorNS::FATAL_ERROR, "Failed to initialize sound system because media file not found."));
		else
		throw(GameError(gameErrorNS::FATAL_ERROR, "Failed to initialize sound system."));
		}*/
	}

	playerObject.camera.setAudio(audio);

	audio->playCue(GAME_MUSIC);

	blockCounter = 0;
	mapMaker maze1("map.txt");
	maze1Height = maze1.getHeight();
	maze1Width  = maze1.getWidth();

	floorBox.init(md3dDevice, maze1Width);

	floor.init(&floorBox, sqrt(2.0f), Vector3(maze1Width - 1,-maze1Width - 1,maze1Width - 1), Vector3(0,0,0), 0, 1.0f);
	floor.setActive();

	ceiling.init(&floorBox, sqrt(2.0f), Vector3(maze1Width - 1, maze1Width + 1.0f ,maze1Width - 1), Vector3(0,0,0), 0, 1.0f);
	ceiling.setActive();

	teenMarker.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	childrensMarker.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	fictionMarker.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	nonfictionMarker.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	historyMarker.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	religionMarker.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);

	teenMarker2.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	childrensMarker2.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	fictionMarker2.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	nonfictionMarker2.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	historyMarker2.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	religionMarker2.init(&markerBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);

	for (int i = 0; i < maze1Height; i++) {
		for (int j = 0; j < maze1Width; j++) {
			if (maze1.getMap()[ (i * maze1Width) + j ] == 'w')
				blockCounter++;
		}
	}

	mazeBlocks = new GameObject[blockCounter];

	for (int i = 0; i < blockCounter; ++i) {
		mazeBlocks[i].init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
		mazeBlocks[i].setInActive();
	}

	door.init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	door.setInActive();

	door2.init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
	door2.setInActive();


	Vector3 currentPosition = Vector3(0, 0, 0);
	int currentBlock = 0;
	Vector3 ghostPos = Vector3(0,0,0);

	for (int i = 0; i < maze1Height; i++) {
		for (int j = 0; j < maze1Width; j++) {
			if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'w'){
				mazeBlocks[currentBlock].setActive();
				mazeBlocks[currentBlock].setPosition(currentPosition);
				currentBlock++;
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'k'){
				key.setActive();
				key.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'd'){
				door.setActive();
				door.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'K'){
				key2.setActive();
				key2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'D'){
				door2.setActive();
				door2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'f'){
				fictionMarker.setActive();
				fictionMarker.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'n'){
				nonfictionMarker.setActive();
				nonfictionMarker.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'r'){
				religionMarker.setActive();
				religionMarker.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'c'){
				childrensMarker.setActive();
				childrensMarker.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'h'){
				historyMarker.setActive();
				historyMarker.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 't'){
				teenMarker.setActive();
				teenMarker.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'F'){
				fictionMarker2.setActive();
				fictionMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'N'){
				nonfictionMarker2.setActive();
				nonfictionMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'R'){
				religionMarker2.setActive();
				religionMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'C'){
				childrensMarker2.setActive();
				childrensMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'H'){
				historyMarker2.setActive();
				historyMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'T'){
				teenMarker2.setActive();
				teenMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'T'){
				teenMarker2.setActive();
				teenMarker2.setPosition(currentPosition);
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'g'){
			
				PatternStepInfo firstGhostPatterns[maxPatternSteps];

				firstGhostPatterns[0] = PatternStepInfo(NONE,2.0f);
				firstGhostPatterns[1] = PatternStepInfo(NONE, 2.0f);
				firstGhostPatterns[2] = PatternStepInfo(NONE, 2.0f);
				firstGhostPatterns[3] = PatternStepInfo(NONE,   2.0f);
				firstGhostPatterns[4] = PatternStepInfo(NONE, 2.0f);

				ghosts[0].init(&ghostBox, sqrt(2.0f), currentPosition, Vector3(0,0,0), GHOST_NORMAL_SPEED, 1.0f, &playerObject, GHOST_RADIUS, firstGhostPatterns);
				ghosts[0].setActive();
				ghosts[0].setAudio(audio);
				
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'G'){
			
				PatternStepInfo firstGhostPatterns[maxPatternSteps];

				firstGhostPatterns[0] = PatternStepInfo(NONE,2.0f);
				firstGhostPatterns[1] = PatternStepInfo(NONE, 2.0f);
				firstGhostPatterns[2] = PatternStepInfo(NONE, 2.0f);
				firstGhostPatterns[3] = PatternStepInfo(NONE,   2.0f);
				firstGhostPatterns[4] = PatternStepInfo(NONE, 2.0f);

				ghosts[1].init(&ghostBox, sqrt(2.0f), currentPosition, Vector3(0,0,0), GHOST_NORMAL_SPEED, 1.0f, &playerObject, GHOST_RADIUS, firstGhostPatterns);
				ghosts[1].setActive();
				ghosts[1].setAudio(audio);
				
			}
			currentPosition.z += 2.0f;
		}
		currentPosition.z = 0.0f;
		currentPosition.x += 2.0f;
	}

	timer = 0.0f;
	ghostChasing = false;
}

void ColoredCubeApp::onResize()
{
	D3DApp::onResize();
	//Camera Object
	playerObject.camera.setPerspective();
	float aspect = (float)mClientWidth/mClientHeight;
	D3DXMatrixPerspectiveFovLH(&mProj, 0.25f*PI, aspect, 1.0f, 1000.0f);
}

void ColoredCubeApp::updateScene(float dt)
{
	drawScene();

	D3DApp::updateScene(dt);

	D3DXMATRIX w;

	score = 0;

	for(int i = 0; i < blockCounter; i++)
		mazeBlocks[i].update(dt);


	floor.update(dt);
	ceiling.update(dt);

	teenMarker.update(dt);
	childrensMarker.update(dt);
	religionMarker.update(dt);
	historyMarker.update(dt);
	fictionMarker.update(dt);
	nonfictionMarker.update(dt);
	teenMarker2.update(dt);
	childrensMarker2.update(dt);
	religionMarker2.update(dt);
	historyMarker2.update(dt);
	fictionMarker2.update(dt);
	nonfictionMarker2.update(dt);

	splashScreen.update(dt);

	if((GetAsyncKeyState(VK_RETURN) & 0x8000) && isOnMenu){
		isOnMenu = false;
		splashScreen.setInActive();
	}

	if((GetAsyncKeyState(VK_SPACE) & 0x8000) && didWin){
		isOnMenu = true;
		timer = 0.0f;
		splashScreen.setActive();
		playerObject.setKey(false);
		key.setActive();
		key2.setActive();
		door.setActive();
		door2.setActive();
		goBackToStart();
		didWin = false;
	}

	if(isOnMenu || didWin){
		playerObject.setPosition(Vector3(-0.5, 0, 2));
	}

	playerObject.update(dt);

	if (!isOnMenu && !didWin){
		timer += dt;

		for(int i = 0; i < NUM_GHOSTS; i++) {
			if (ghosts[i].collided(&playerObject))
			{
				if (lightIsOnGhost(&ghosts[i]))
					ghosts[i].bounce(&playerObject);
				else 
					goBackToStart();

			}
			ghosts[i].update(dt);
		}

		key.update(dt);
		door.update(dt);
		key2.update(dt);
		door2.update(dt);

		for (int i = 0; i < blockCounter; i++) {
			if (playerObject.collided(&mazeBlocks[i])) {
				playerObject.bounce(&mazeBlocks[i]);
				playerObject.setCollision(true);
			}
			
		}

		if(playerObject.collided(&key)){
			playerObject.setKey(true);
			key.setInActive();
			audio->playCue(KEYS);
		}

		if(playerObject.collided(&key2)){
			playerObject.setKey(true);
			key2.setInActive();
			audio->playCue(KEYS);
		}

		if(playerObject.collided(&door) && (!playerObject.getKey())){
			playerObject.bounce(&door);
			playerObject.setCollision(true);
		}
		else if(playerObject.collided(&door) && playerObject.getKey()){
			door.setInActive();
			audio->playCue(OPEN);
			playerObject.setKey(false);
		}

		if(playerObject.collided(&door2) && (!playerObject.getKey())){
			playerObject.bounce(&door2);
			playerObject.setCollision(true);
		}
		else if(playerObject.collided(&door2) && playerObject.getKey()){
			door2.setInActive();
			audio->playCue(OPEN);
			playerObject.setKey(false);
			didWin = true;
		}

		int rand = (int)(10000*dt) % 3;

		for (int i = 0; i < NUM_GHOSTS; i++) {
		int chaseCounter = 0;
		ghostChasing = false;
		for (int i = 0; i < NUM_GHOSTS; ++i) {
			if ( ghosts[i].getShouldTrack() && !ghostChasing) {
				switch (rand) {
				case 0:
					audio->playCue(GHOST_CHASE1);
					break;
				case 1:
					audio->playCue(GHOST_CHASE2);
					break;
				case 2:
					audio->playCue(GHOST_CHASE3);
					break;
				}

				ghostChasing = true;
			}

			else
				chaseCounter++;
		}

		if (chaseCounter == 2) {
			audio->stopCue(GHOST_CHASE1);
			audio->stopCue(GHOST_CHASE2);
			audio->stopCue(GHOST_CHASE3);
			ghostChasing = false;
		}

	}

	//Get Camera viewMatrix
	mView = playerObject.camera.getViewMatrix();
	mProj = playerObject.camera.getProjectionMatrix();

	// The spotlight takes on the camera position and is aimed in the
	// same direction the camera is looking.  In this way, it looks
	// like we are holding a flashlight.
	mLights[2].pos = playerObject.getPosition();
	D3DXVec3Normalize(&mLights[2].dir, &(playerObject.camera.getLookAt()-playerObject.getPosition()));

}


void ColoredCubeApp::drawScene()
{

	D3DApp::drawScene();

	// Restore default states, input layout and primitive topology 
	// because mFont->DrawText changes them.  Note that we can 
	// restore the default states by passing null.
	md3dDevice->OMSetDepthStencilState(0, 0);
	float blendFactors[] = {0.0f, 0.0f, 0.0f, 0.0f};
	md3dDevice->OMSetBlendState(0, blendFactors, 0xffffffff);
	md3dDevice->IASetInputLayout(mVertexLayout);
	md3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Get Camera viewMatrix
	mView = playerObject.camera.getViewMatrix();
	mProj = playerObject.camera.getProjectionMatrix();

	// Set per frame constants.
	mfxEyePosVar->SetRawValue(&playerObject.camera.getPosition(), 0, sizeof(D3DXVECTOR3));
	mfxFlashLightVar->SetRawValue(&mLights[mLightType], 0, sizeof(Light));
	mfxLightType->SetInt(mLightType);
	mfxDiffuseMapVar->SetResource(mDiffuseMapRV);
	mfxSpecMapVar->SetResource(mSpecMapRV);

	// set constants
	mWVP = mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);

	D3DXMATRIX texMtx;
	D3DXMatrixIdentity(&texMtx);
	mfxTexMtxVar->SetMatrix((float*)&texMtx);


	D3D10_TECHNIQUE_DESC techDesc;
	mTech->GetDesc( &techDesc );



	for(UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex( p )->Apply(0);
	}

	for(int i = 0; i < blockCounter; i++){
		mWVP = mazeBlocks[i].getWorldMatrix()  *mView*mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&mazeBlocks[i].getWorldMatrix());
		mazeBlocks[i].setMTech(mTech);
		mazeBlocks[i].draw();
	}

	mfxDiffuseMapVar->SetResource(ghostTex);

	int foo[1] = {0};
	for(int i = 0; i < NUM_GHOSTS; i++){
		if (!lightIsOnGhost(&ghosts[i])) {
			foo[0] = 0;
		} else {
			foo[0] = 1;
		}
		mfxFlipGhostVar->SetRawValue(&foo[0], 0, sizeof(int));

		mWVP = ghosts[i].getWorldMatrix()  *mView*mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&ghosts[i].getWorldMatrix());
		ghosts[i].setMTech(mTech);
		ghosts[i].draw();

		foo[0] = 0;
		mfxFlipGhostVar->SetRawValue(&foo[0], 0, sizeof(int));
	}

	mfxDiffuseMapVar->SetResource(floorTex);

	mWVP = floor.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&floor.getWorldMatrix());
	floor.setMTech(mTech);
	floor.draw();

	mfxDiffuseMapVar->SetResource(doorTex);

	mWVP = door.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&door.getWorldMatrix());
	door.setMTech(mTech);
	door.draw();

	mWVP = door2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&door2.getWorldMatrix());
	door2.setMTech(mTech);
	door2.draw();

	mfxDiffuseMapVar->SetResource(ceilingTex);

	mWVP = ceiling.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&ceiling.getWorldMatrix());
	ceiling.setMTech(mTech);
	ceiling.draw();

	mfxDiffuseMapVar->SetResource(keyTex);

	mWVP = key.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&key.getWorldMatrix());
	key.setMTech(mTech);
	key.draw();

	mWVP = key2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&key2.getWorldMatrix());
	key2.setMTech(mTech);
	key2.draw();

	mfxDiffuseMapVar->SetResource(fictionTex);

	mWVP = fictionMarker.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&fictionMarker.getWorldMatrix());
	fictionMarker.setMTech(mTech);
	fictionMarker.draw();

	mWVP = fictionMarker2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&fictionMarker2.getWorldMatrix());
	fictionMarker2.setMTech(mTech);
	fictionMarker2.draw();

	mfxDiffuseMapVar->SetResource(nonfictionTex);

	mWVP = nonfictionMarker.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&nonfictionMarker.getWorldMatrix());
	nonfictionMarker.setMTech(mTech);
	nonfictionMarker.draw();

	mWVP = nonfictionMarker2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&nonfictionMarker2.getWorldMatrix());
	nonfictionMarker2.setMTech(mTech);
	nonfictionMarker2.draw();

	mfxDiffuseMapVar->SetResource(religionTex);

	mWVP = religionMarker.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&religionMarker2.getWorldMatrix());
	religionMarker.setMTech(mTech);
	religionMarker.draw();

	mWVP = religionMarker2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&religionMarker2.getWorldMatrix());
	religionMarker2.setMTech(mTech);
	religionMarker2.draw();

	mfxDiffuseMapVar->SetResource(historyTex);

	mWVP = historyMarker.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&historyMarker.getWorldMatrix());
	historyMarker.setMTech(mTech);
	historyMarker.draw();

	mWVP = historyMarker2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&historyMarker2.getWorldMatrix());
	historyMarker2.setMTech(mTech);
	historyMarker2.draw();

	mfxDiffuseMapVar->SetResource(childrensTex);

	mWVP = childrensMarker2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&childrensMarker2.getWorldMatrix());
	childrensMarker2.setMTech(mTech);
	childrensMarker2.draw();

	mWVP = childrensMarker.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&childrensMarker.getWorldMatrix());
	childrensMarker.setMTech(mTech);
	childrensMarker.draw();

	mfxDiffuseMapVar->SetResource(teenTex);

	mWVP = teenMarker2.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&teenMarker2.getWorldMatrix());
	teenMarker2.setMTech(mTech);
	teenMarker2.draw();

	mWVP = teenMarker.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&teenMarker.getWorldMatrix());
	teenMarker.setMTech(mTech);
	teenMarker.draw();

	mfxDiffuseMapVar->SetResource(splashTex);

	mWVP = splashScreen.getWorldMatrix()  *mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);
	mfxWorldVar->SetMatrix((float*)&splashScreen.getWorldMatrix());
	splashScreen.setMTech(mTech);
	splashScreen.draw();

	// We specify DT_NOCLIP, so we do not care about width/height of the rect.
	if(isOnMenu){
		std::wostringstream newString;   
		newString << "                            Instructions:\n";
		newString << "Find the Golden keys to open the Exit doors.\n";
		newString << "Traverse the mazes and find the Doors.\n";
		newString << "\n\n\n\n\n\n";
		newString << "But watch out for Ghosts! If a ghost catches you\n";
		newString << "          You will be sent back to the start\n";
		finalScore = newString.str();
		RECT R1 = {125, 20, 0, 0};
		mFont->DrawText(0, finalScore.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}
	if(didWin){
		std::wostringstream newString;

		newString.setf(std::ios::fixed);
		newString.precision(0);

		newString << "YOU WIN\n";
		newString << "Time: ";
		newString << "Hit space to restart\n";

		int minutes = (int) timer/60;
		float remainder = timer - minutes*60;

		newString << minutes;
		newString << ':';

		if (remainder < 10.f) {
			newString << '0';
			newString << (int)remainder;
		}

		else
			newString << (int) remainder;


		finalScore = newString.str();
		RECT R1 = {325, 200, 0, 0};
		mFont->DrawText(0, finalScore.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}
	else {
		std::wostringstream key;
		if (playerObject.getKey()) {
			key << "Key";
		}
		else key << "";

		std::wostringstream clock;

		clock.setf(std::ios::fixed);
		clock.precision(0);

		if (timer < 10.f) {
			clock << '0';
			clock << ':';
			clock << '0';
			clock << timer;
		}

		else {

			int minutes = (int) timer/60;
			float remainder = timer - minutes*60;

			clock << minutes;
			clock << ':';

			if (remainder < 10.f) {
				clock << '0';
				clock << (int)remainder;
			}
			else
				clock << (int) remainder;
		}

		RECT R1 = {100, 500, 0, 0};
		RECT R2 = {370, 500, 0, 0};

		std::wstring wkey = key.str();
		std::wstring time_clock = clock.str();
		mFont->DrawText(0, time_clock.c_str(), -1, &R2, DT_NOCLIP, WHITE);
		keyFont->DrawText(0, wkey.c_str(), -1, &R1, DT_NOCLIP, YELLOW);
	}

	mSwapChain->Present(0, 0);
}

//to be made later possibly
void ColoredCubeApp::restartGame() { }

void ColoredCubeApp::buildFX()
{
	DWORD shaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

	ID3D10Blob* compilationErrors = 0;
	HRESULT hr = 0;


	hr = D3DX10CreateEffectFromFile(L"lighting.fx", 0, 0, 
		"fx_4_0", shaderFlags, 0, md3dDevice, 0, 0, &mFX, &compilationErrors, 0);
	if(FAILED(hr))
	{
		if( compilationErrors )
		{
			MessageBoxA(0, (char*)compilationErrors->GetBufferPointer(), 0, 0);
			ReleaseCOM(compilationErrors);
		}
		DXTrace(__FILE__, (DWORD)__LINE__, hr, L"D3DX10CreateEffectFromFile", true);
	} 


	mTech = mFX->GetTechniqueByName("LightTech");

	mfxWVPVar    = mFX->GetVariableByName("gWVP")->AsMatrix();
	mfxWorldVar  = mFX->GetVariableByName("gWorld")->AsMatrix();
	mfxEyePosVar = mFX->GetVariableByName("gEyePosW");
	mfxFlashLightVar  = mFX->GetVariableByName("gLight1");
	mfxLightType = mFX->GetVariableByName("gLightType1")->AsScalar();
	mfxDiffuseMapVar = mFX->GetVariableByName("gDiffuseMap")->AsShaderResource();
	mfxSpecMapVar    = mFX->GetVariableByName("gSpecMap")->AsShaderResource();
	mfxTexMtxVar     = mFX->GetVariableByName("gTexMtx")->AsMatrix();

	mfxFlipGhostVar = mFX->GetVariableByName("flip");
}

void ColoredCubeApp::buildVertexLayouts()
{
	// Create the vertex input layout.
	D3D10_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 0,  D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"DIFFUSE",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"SPECULAR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 40, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};

	// Create the input layout
	D3D10_PASS_DESC PassDesc;
	mTech->GetPassByIndex(0)->GetDesc(&PassDesc);
	HR(md3dDevice->CreateInputLayout(vertexDesc, 5, PassDesc.pIAInputSignature,
		PassDesc.IAInputSignatureSize, &mVertexLayout));
}

void ColoredCubeApp::buildMaze(mapMaker maze){

}

bool ColoredCubeApp::lightIsOnGhost(Ghost* ghost) {
	Vector3 lookAtRay = playerObject.camera.getLookAt() - (playerObject.getPosition() - playerObject.originalPos);

	//Transform(&lookAtRay, &lookAtRay, &(playerObject.camera.getProjectionMatrix() )); 
	D3DXVec3Normalize(&lookAtRay, &( lookAtRay ));
	Vector3 rayPosition = playerObject.getPosition();


	_RPT1(0,"lookAt x %f " , lookAtRay.x);
	_RPT1(0,"lookAt y %f " , lookAtRay.y);
	_RPT1(0,"lookAt z %f\n", lookAtRay.z);

	Vector3 ghostPosition = ghost->getPosition();

	//ray march to figure out if it hits the ghost
	lookAtRay /= 5;
	float distance;
	while (true) {
		distance = D3DXVec3Length(&(ghostPosition - rayPosition));
		if ( distance < ghost->getRadius() ) 
		{
			ghost->setSpeed(GHOST_LIGHT_SPEED);
			return true;
		} else if (distance > 10.0f) {
			ghost->setSpeed(GHOST_NORMAL_SPEED);
			return false;
		}
		rayPosition += lookAtRay;
	}
}

void ColoredCubeApp::goBackToStart() {
	audio->playCue(PLAYER_DIE);
	playerObject.setPosition(PLAYER_START_LVL1);
	playerObject.camera.init(playerObject.getPosition(), Vector3(0,0,0), INITIAL_LOOKAT);
}
