#include "Ghost.h"

void Ghost::init(TexBox *b, float r, Vector3 pos, Vector3 vel, float sp, float s, PlayerObject* player, float trackRadius, PatternStepInfo p[])
{
	GameObject::init(b, r, pos, vel, sp, s);
	direction = Vector3(0,0,0);
	timeInStep = 0.0f;
	this->player = player;
	patternIndex = 0;
	this->trackRadius = trackRadius;
	shouldReturn = false;
	origin = pos;
	for (int i = 0; i < maxPatternSteps; i++) {
		patternSteps[i] = p[i];
	}
}

Ghost::Ghost() {

}

void Ghost::update(float dt){
	//RotateY(&GameObject::getWorldMatrix(), angle);
	updatePatterns(dt);
	GameObject::setVelocity(direction * GameObject::getSpeed());
	GameObject::update(dt);
}

void Ghost::updatePatterns(float dt)
{

	if (shouldTrack()) {
		shouldReturn = true;
		currentAction = TRACK;

	} else if (shouldReturn) {
		timeInStep = 0;
		currentAction = RETURN;
	}
	else {
		timeInStep += dt;

		if (timeInStep > patternSteps[patternIndex].timeForStep)
		{
			timeInStep = 0;
			patternIndex++;
			//should just loop back to the beginning
			if (patternIndex == maxPatternSteps) {
				patternIndex = 0;
			}
		}

		currentAction = patternSteps[patternIndex].action;
	}
	//if over time has ellapsed, need to go to next index

	switch (currentAction)
	{
	case NONE:
		break;
	case UP:
		direction = Vector3(0, 0, 1);
		//TODO...set angles
		break;
	case DOWN:
		direction = Vector3(0, 0, -1);
		break;
	case RIGHT:
		direction = Vector3(1, 0, 0);
		break;
	case LEFT:
		direction = Vector3(-1, 0, 0);
		break;
	case TRACK:
		vectorTrack();
		break;
	case RETURN:
		returnToOrigin();
		break;
	}
}

void Ghost::vectorTrack() {

	direction = player->getPosition() - GameObject::getPosition();
	if(direction.x == 0 && direction.z == 0)
		return;

	D3DXVec3Normalize(&direction, &direction);

	angle = atan2(direction.x, direction.z);


	/*if (angleVector.x < 0)
	angle = 2 * PI - angle;*/
}

void Ghost::returnToOrigin() {
	direction = origin - GameObject::getPosition();
	setSpeed(GHOST_NORMAL_SPEED);
	if (direction.x == 0 && direction.z == 0) {
		shouldReturn = false;
		return;
	}
	D3DXVec3Normalize(&direction, &direction);
}

void Ghost::bounce(GameObject *go) {
	Vector3 collisionVector = go->getPosition() - getPosition();

	//normalize the collision vec and make it super small so that it's not shaky
	collisionVector = *D3DXVec3Normalize(&collisionVector, &collisionVector)/200;

	int count = 30;
	do {
		setPosition(getPosition() - collisionVector);
		count--;
	} while (GameObject::collided(go) && count);
}
